import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.newForm();
  }

  
  newForm(): void{
       this.contactForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['',[Validators.required, Validators.email]],
      subject: ['', [Validators.required,Validators.minLength(8)]],
      message: ['',Validators.required], 
    });
  }
 get c(): any {
    return this.contactForm.controls;
  }
  onSubmit(){
    this.submitted = true;
    if(this.contactForm.invalid){
      return;
    }
  }
}
