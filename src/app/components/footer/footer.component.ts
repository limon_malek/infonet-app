import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  contactForm: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder) { }
  ngOnInit(): void {
    this.newForm();
  }

  newForm(): void {
    this.contactForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  get c(): any {
    return this.contactForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.contactForm.invalid) {
      return;
    }
    console.log(this.contactForm.value)
    this.contactForm.reset();
    this.submitted = false;
  }

}
