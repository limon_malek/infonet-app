import { Component, HostListener, OnInit } from '@angular/core';
import { ModalService } from 'src/app/shared/modal/modal.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  isToggleIcon = true;
  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
  }
  openModal(id: string) {
    this.isToggleIcon = false;
    console.log("open modal");
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
    this.isToggleIcon = true
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    if (document.body.offsetWidth > 991 || document.documentElement.offsetWidth > 991) {
      this.closeModal('mobileNav__modal');
    }
  }
}
