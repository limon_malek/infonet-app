import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})
export class AboutPageComponent implements OnInit {
  hero = {
    title: "ABOUT US",
    subTitle: "",
    imageUrl: "../../../../assets/img/services/service.jpg"
  }
  constructor() { }

  ngOnInit(): void {
  }

}
