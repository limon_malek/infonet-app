import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-career-page',
  templateUrl: './career-page.component.html',
  styleUrls: ['./career-page.component.css']
})
export class CareerPageComponent implements OnInit {
  hero = {
    title: "Career",
    subTitle: "Come Work With Us",
    imageUrl: "../../../../assets/img/services/service.jpg"
  }
  constructor() { }

  ngOnInit(): void {
  }

}
