import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit {
  hero = {
    title: "CONTACT US",
    subTitle: "",
    imageUrl: "../../../../assets/img/services/service.jpg"
  }
  constructor() { }

  ngOnInit(): void {
  }

}
