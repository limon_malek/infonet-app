import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal/modal.component';
import { HeroBannerComponent } from './components/hero-banner/hero-banner.component';



@NgModule({
  declarations: [
    ModalComponent,
    HeroBannerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ModalComponent,
    HeroBannerComponent
  ]
})
export class SharedModule { }
